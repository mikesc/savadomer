use crate::save::{PC_DNG_LVL_POS, PC_DNG_POS, PC_POS_X_IDX, PC_POS_Y_IDX, Save, LVL_VISITED_FLAGS_POS};
use crate::save::{GDSD, HDR_SIZE, LVMP};

const STATUE_FEATURE: u32 = 0x26;
const FORGE_FEATURE: u32 = 0xe;
const POOL_FEATURE: u32 = 0xd;
const ALTAR_FEATURE: u32 = 5;
const FEATU_SIZE_IN_SAVE: u32 = 6;

const LVNF_TO_FLAGS_OFFSET: usize = 62;
const LVNF_TO_ALTAR_ALIGNMENT_OFFSET: usize = 101;
const LVNF_TO_STATUE_ID_OFFSET: usize = 107;
const LVNF_TO_STATUE_DISABLED_OFFSET: usize = 109;
const LVNF_TO_STATUE_X: usize = 113;
const LVNF_TO_STATUE_Y: usize = 117;
const LVNF_TO_STATUE_TIMOUT: usize = 121;
const LVNF_STATUE_FLAG: u32 = 8192;
const LVNF_POOL_FLAG: u32 = 32;
const LVNF_FORGE_FLAG: u32 = 8;
const LVNF_ALTAR_FLAG: u32 = 0x4;

const LVNF_TILE_SIZE: usize = 1;
const LVNF_DIRECTION_SIZE: usize = 1;
const LVNF_FLAG_SIZE: usize = 2;
const LVNF_KNOWN_TILE_SIZE: usize = 8;
const LVNF_KNOWN_FEATURE_SIZE: usize = 8;
const LVNF_KNOWN_ITEM_SIZE: usize = 28;
const LVNF_VISIBILITY_SIZE: usize = 1;
const LVNF_OLD_VISIBILITY_SIZE: usize = 1;
const LVNF_MAPS_SIZE: usize = LVNF_TILE_SIZE + LVNF_DIRECTION_SIZE +
    LVNF_FLAG_SIZE + LVNF_KNOWN_TILE_SIZE +
    LVNF_KNOWN_FEATURE_SIZE + LVNF_KNOWN_ITEM_SIZE +
    LVNF_VISIBILITY_SIZE + LVNF_OLD_VISIBILITY_SIZE;

const PLYR_VISITED_SIZE: usize = 4;

const MIN_EDL: i16 = 1;
const MAX_EDL: i16 = 50;

pub(crate) const SMALL_VAULT:u32 = 0x12;
pub(crate) const BIG_VAULT:u32 = 0x13;
const STANDARD_LEVEL:u32 = 0xFFFFFFFF;

const ALIGNMENT_NAMES: [&str; 3] = [
    "chaotic",
    "neutral",
    "lawfull"
];

const STATUE_MAX_IDX: u16 = 268;

const BUG_TEMPLE_ENTRY_OFFSET:usize = 4760625;
const DEATHS_TO_UNLOCK_BUG_TEMPLE:u32 = 100;

pub(crate) fn print_levels(save: &Save) {
    for lvl in 0..6 {
        for dng in 1..51 {
            let lvnf_offset = save.get_lvl_dng_lvnf_offset(lvl, dng);
            let level_type = save.u32_from_save(lvnf_offset + HDR_SIZE);
            let lvnf_edl = save.u16_from_save(lvnf_offset + HDR_SIZE + 4) as i16;
            let visited = save.u32_from_save(LVL_VISITED_FLAGS_POS +
                (dng * 100 + lvl) as usize * PLYR_VISITED_SIZE);

            let id = lvl * 100 + dng;
            if lvnf_edl >= MIN_EDL && lvnf_edl <= MAX_EDL {
                println!("ID: {id:03}, EDL: {lvnf_edl:3}, Name: {:33}, Type: {}, Visited: {visited}",
                         lvl_name(lvl, dng), lvl_type_desc(level_type));
            } else {
                println!("ID: {id:03} is not used");
            }
        }
    }
}

pub(crate) fn print_vault_levels(save: &Save) {
    for lvl in 0..6 {
        for dng in 1..51 {
            let lvnf_offset = save.get_lvl_dng_lvnf_offset(lvl, dng);
            let level_type = save.u32_from_save(lvnf_offset + HDR_SIZE) as u32;

            if level_type != STANDARD_LEVEL && level_type != SMALL_VAULT && level_type != BIG_VAULT {
                continue;
            }

            let lvnf_edl = save.u16_from_save(lvnf_offset + HDR_SIZE + 4) as i16;
            let visited = save.u32_from_save(LVL_VISITED_FLAGS_POS +
                (dng * 100 + lvl) as usize * PLYR_VISITED_SIZE);

            let id = lvl * 100 + dng;
            if lvnf_edl >= MIN_EDL && lvnf_edl <= MAX_EDL && visited == 0 {
                println!("ID: {id:03}, EDL: {lvnf_edl:3}, Name: {:33}, Type: {}, Visited: {visited}",
                         lvl_name(lvl, dng), lvl_type_desc(level_type));
            }
        }
    }
}

pub(crate) fn lvl_name(lvl: u16, dng: u16) -> &'static str {
    let lvl_id = lvl * 100 + dng;
    let name = match lvl_id {
        1..=50    => "Caverns of Chaos",
        101       => "CoC shortcut",
        102..=103 => "Dwarven Halls",
        104       => "Word map",
        105       => "Terinyo",
        106       => "Water Dragon Cave",
        107..=108 => "Dwarven Graveyard",
        109..=115 => "Carpenter dung",
        116       => "Tomb of the High Kings (last)",
        117       => "Cave of the gremlins",
        118..=119 => "Darkforge",
        120       => "Beautiful Park",
        121       => "Corrupted Glade",
        122       => "Bandit city",
        123..=126 => "Tower of Ethernal Flames",
        127       => "Clearing of the Mad Minstrel",
        128       => "Small cave",
        129       => "Comfortable clearing",
        130..=132 => "Pyramid",
        133       => "Ogre tribe cave",
        134..=142 => "The Rift",
        143       => "Library",
        144       => "Infinite dungeon",
        145       => "Wilderness",
        146       => "Strange tree dungeon",
        147       => "Realm of the chaos dragon",
        148       => "Very special part of infinity",
        149..=150 => "Assassin Guild",
        201..=207 => "Druid Dungeon",
        208..=215 => "Unremarkable dungeon",
        216       => "High mountain village",
        217       => "Chaos Plane",
        218..=222 => "Tomb of the High Kings",
        223..=228 => "Puppy Cave",
        229..=234 => "Unreal Cave",
        235       => "Mana Temple",
        236..=240 => "Blue Dragon Caves",
        241       => "Assassin Guild (last)",
        242..=250 => "The Scintillating Cave",
        301       => "Minotaur City",
        302..=308 => "Minotaur Cave (good)",
        309..=322 => "Minotaur Cave (fake)",
        323..=324 => "Bug Temple",
        325       => "Ice Queen Domain (prison)",
        326..=328 => "Unassuming Cave",
        329..=338 => "Ice Queen Domain",
        339       => "Arena of ChAoS",
        340..=345 => "Caves of the frost giant jarl",
        346..=350 => "Moldy dungeon",
        401..=412 => "Shadowy dungeon",
        413..=417 => "Sinister dungeon",
        418..=430 => "Antediluvian dungeon",
        431..=433 => "Putrid cave",
        434..=440 => "Crumbling dungeon",
        501..=505 => "Great stone wyrm caverns",
        506       => "Eerie Glare",
        507..=510 => "Antediluvian Dwarven Dungeon",
        511..=514 => "Rolf Fortress",
        515..=530 => "Volcanic caves",
        531..=533 => "Fungal Caves",
        534       => "Goblin camp",
        535..=545 => "Caverns of the ultimate challenge",
        546       => "Borderland settlement",
        547       => "Merchant Guild",
        548       => "Thieves guild tunels",
        549       => "Heavenly Area",
        _         => "Unused"
    };
    return name;
}

fn lvl_type_desc(lvnf_type: u32) -> &'static str {
    let desc = match lvnf_type {
        STANDARD_LEVEL => "default",
        SMALL_VAULT => "small vault",
        BIG_VAULT => "greater vault",
        _ => "custom"
    };
    return desc;
}

pub(crate) fn create_vault(save: &mut Save, lvl_id: u16, vault_type: u32) {
    let lvl = lvl_id / 100;
    let dng = lvl_id % 100;

    if lvl > 5 || dng > 50 {
        println!("Bad level id. Please use option --new-vault-levels to see eligible levels.");
        return;
    }

    let visited = save.u32_from_save(LVL_VISITED_FLAGS_POS +
        (dng * 100 + lvl) as usize * PLYR_VISITED_SIZE);

    if visited != 0 {
        println!("There is no point in changing level type to already generated level./n
                 Please use option --new-vault-levels to see eligible levels.");
        return;
    }

    let lvnf_offset = save.get_lvl_dng_lvnf_offset(lvl, dng);
    let level_type = save.u32_from_save(lvnf_offset + HDR_SIZE);
    if level_type == STANDARD_LEVEL || level_type == SMALL_VAULT || level_type == BIG_VAULT {
        println!("Changing level to {}", lvl_type_desc(vault_type));
        save.update_u32_in_save(vault_type, lvnf_offset + HDR_SIZE);
    } else {
        println!("Will not change this level type. Please use option --new-vault-levels to see eligible levels.");
    }
}

pub(crate) fn unlock_bug_temple(save: &mut Save) {
    if save.u32_from_save(BUG_TEMPLE_ENTRY_OFFSET) >= DEATHS_TO_UNLOCK_BUG_TEMPLE {
        println!("Bug temple is already unlocked.");
    } else {
        save.update_u32_in_save(100, BUG_TEMPLE_ENTRY_OFFSET);
        println!("Bug temple unlocked.");
    }
}

pub(crate) fn create_statue(save: &mut Save, new_statue_id: Option<u16>) {
    if let Some(statue_id) = new_statue_id {
        if statue_id > STATUE_MAX_IDX {
            println!("Couldn't find '{statue_id}'. Use correct id from 0 to {STATUE_MAX_IDX}. Statue ids can be found on https://gitlab.com/mikesc/savadomer/-/blob/master/doc/statues.md?ref_type=heads");
            return;
        }

        let pc_pos_x = save.u32_from_save(PC_POS_X_IDX);
        let pc_pos_y = save.u32_from_save(PC_POS_Y_IDX);
        let pc_lvl = save.u16_from_save(PC_DNG_LVL_POS);
        let pc_dng = save.u16_from_save(PC_DNG_POS);

        let (lvl_features_count_idx, lvl_features_count, lvl_features_idx) =
            get_feature_table_info(save, pc_lvl, pc_dng);

        let moved = move_feature_to(save, pc_pos_x, pc_pos_y, lvl_features_count, lvl_features_idx, STATUE_FEATURE);
        if moved == true {
            println!("Moved statue to current PC position.");
            return;
        }
        let added = add_feature_at(save, pc_pos_x, pc_pos_y, lvl_features_count,
                                   lvl_features_idx, lvl_features_count_idx, STATUE_FEATURE);
        if added == true {
            println!("Added statue to current PC position.");
        } else {
            println!("There is other feature at current PC position. Statue was not added.");
            return;
        }

        //Update level flags
        let lvnf_offset = save.get_lvl_dng_lvnf_offset(pc_lvl, pc_dng);
        let old_flags = save.u32_from_save(lvnf_offset + LVNF_TO_FLAGS_OFFSET);
        save.update_u32_in_save(old_flags | LVNF_STATUE_FLAG, lvnf_offset + LVNF_TO_FLAGS_OFFSET);
        save.update_u16_in_save(statue_id, lvnf_offset + LVNF_TO_STATUE_ID_OFFSET);
        save.update_u32_in_save(0, lvnf_offset + LVNF_TO_STATUE_DISABLED_OFFSET);
        save.update_u32_in_save(pc_pos_x, lvnf_offset + LVNF_TO_STATUE_X);
        save.update_u32_in_save(pc_pos_y, lvnf_offset + LVNF_TO_STATUE_Y);
        save.update_u32_in_save(0, lvnf_offset + LVNF_TO_STATUE_TIMOUT);

    }
}

pub(crate) fn create_forge(save: &mut Save) {
    let pc_pos_x = save.u32_from_save(PC_POS_X_IDX);
    let pc_pos_y = save.u32_from_save(PC_POS_Y_IDX);
    let pc_lvl = save.u16_from_save(PC_DNG_LVL_POS);
    let pc_dng = save.u16_from_save(PC_DNG_POS);

    let (lvl_features_count_idx, lvl_features_count, lvl_features_idx) =
        get_feature_table_info(save, pc_lvl, pc_dng);
    let added = add_feature_at(save, pc_pos_x, pc_pos_y, lvl_features_count,
                               lvl_features_idx, lvl_features_count_idx, FORGE_FEATURE);

    if added == true {
        //Update level flags
        let lvnf_offset = save.get_lvl_dng_lvnf_offset(pc_lvl, pc_dng);
        let old_flags = save.u32_from_save(lvnf_offset + LVNF_TO_FLAGS_OFFSET);
        save.update_u32_in_save(old_flags | LVNF_FORGE_FLAG, lvnf_offset + LVNF_TO_FLAGS_OFFSET);
        println!("Added forge to current PC position.");
    } else {
        println!("There is other feature at current PC position. Forge was not added.");
    }
}

pub(crate) fn create_pool(save: &mut Save) {
    let pc_pos_x = save.u32_from_save(PC_POS_X_IDX);
    let pc_pos_y = save.u32_from_save(PC_POS_Y_IDX);
    let pc_lvl = save.u16_from_save(PC_DNG_LVL_POS);
    let pc_dng = save.u16_from_save(PC_DNG_POS);

    let (lvl_features_count_idx, lvl_features_count, lvl_features_idx) =
        get_feature_table_info(save, pc_lvl, pc_dng);
    let added = add_feature_at(save, pc_pos_x, pc_pos_y, lvl_features_count,
                               lvl_features_idx, lvl_features_count_idx, POOL_FEATURE);

    if added == true {
        //Update level flags
        let lvnf_offset = save.get_lvl_dng_lvnf_offset(pc_lvl, pc_dng);
        let old_flags = save.u32_from_save(lvnf_offset + LVNF_TO_FLAGS_OFFSET);
        save.update_u32_in_save(old_flags | LVNF_POOL_FLAG, lvnf_offset + LVNF_TO_FLAGS_OFFSET);
        println!("Added pool to current PC position.");
    } else {
        println!("There is other feature at current PC position. Pool was not added.");
    }
}

pub(crate) fn create_altar(save: &mut Save, altar_alignment: &String) {
    let alignment = parse_altar_alignment(altar_alignment);
    if alignment >= ALIGNMENT_NAMES.len() {
        println!("Couldn't find '{altar_alignment}'. Use correct id from 0 to {0} or alignment name (chaotic, neutral, lawfull).", ALIGNMENT_NAMES.len()-1);
        return;
    }

    let pc_pos_x = save.u32_from_save(PC_POS_X_IDX);
    let pc_pos_y = save.u32_from_save(PC_POS_Y_IDX);
    let pc_lvl = save.u16_from_save(PC_DNG_LVL_POS);
    let pc_dng = save.u16_from_save(PC_DNG_POS);

    let (lvl_features_count_idx, lvl_features_count, lvl_features_idx) =
        get_feature_table_info(save, pc_lvl, pc_dng);

    let lvnf_offset = save.get_lvl_dng_lvnf_offset(pc_lvl, pc_dng);
    let alignment = alignment as i32 - 1;
    save.update_u32_in_save(alignment as u32, lvnf_offset + LVNF_TO_ALTAR_ALIGNMENT_OFFSET);

    let moved = move_feature_to(save, pc_pos_x, pc_pos_y, lvl_features_count, lvl_features_idx, ALTAR_FEATURE);
    if moved == true {
        println!("Moved altar to current PC position.");
        return;
    }
    let added = add_feature_at(save, pc_pos_x, pc_pos_y, lvl_features_count,
                               lvl_features_idx, lvl_features_count_idx, ALTAR_FEATURE);
    if added == true {
        //Update level flags
        let old_flags = save.u32_from_save(lvnf_offset + LVNF_TO_FLAGS_OFFSET); //LVNF_ALTAR_FLAG
        save.update_u32_in_save(old_flags | LVNF_ALTAR_FLAG, lvnf_offset + LVNF_TO_FLAGS_OFFSET);
        println!("Added altar to current PC position.");
    } else {
        println!("There is other feature at current PC position. Altar was not added.");
        return;
    }

}

pub(crate) fn print_dungeon_level_info(save: &Save) {
    let pc_lvl = save.u16_from_save(PC_DNG_LVL_POS);
    let pc_dng = save.u16_from_save(PC_DNG_POS);

    let lvnf_offset = save.get_lvl_dng_lvnf_offset(pc_lvl, pc_dng);
    let lvnf_type = save.u32_from_save(lvnf_offset + HDR_SIZE);
    let lvnf_edl = save.u16_from_save(lvnf_offset + HDR_SIZE + 4);

    let id = pc_lvl * 100 + pc_dng;
    println!("ID: {id:03}");
    println!("LVNF type: {lvnf_type} ({})", lvl_type_desc(lvnf_type));
    println!("EDL: {lvnf_edl}");
    println!("Name: {}", lvl_name(pc_lvl, pc_dng));
}

fn parse_altar_alignment(name_or_number: &str) -> usize {
    let alignment = match name_or_number.parse() {
        Ok(number) => number,
        Err(_) => ALIGNMENT_NAMES.iter().position(|&r| r == name_or_number.to_lowercase()).unwrap_or_else(||usize::MAX),
    };
    alignment
}

fn get_feature_table_info(save: &Save, pc_lvl:u16, pc_dng:u16) -> (usize, u32, usize) {
    let display_offset = save.get_section_offset(GDSD, 0);
    let lines = (save.u32_from_save(display_offset + HDR_SIZE) - 5) as usize;
    let columns = save.u32_from_save(display_offset + HDR_SIZE + 4) as usize;

    let lvmp_lvl_idx = save.get_lvl_dng_lvmp_index(pc_lvl, pc_dng);

    let lvmps = save.get_sections_of_type(LVMP);
    let level_start_idx = *lvmps.get(lvmp_lvl_idx).unwrap();

    let lvl_features_count_idx = level_start_idx + HDR_SIZE + 4 + (lines * columns) * LVNF_MAPS_SIZE;
    let lvl_features_count = save.u32_from_save(lvl_features_count_idx);
    let lvl_features_idx = level_start_idx + HDR_SIZE + 4 + (lines * columns) * LVNF_MAPS_SIZE + 4;
    (lvl_features_count_idx, lvl_features_count, lvl_features_idx)
}

fn move_feature_to(save: &mut Save, pc_pos_x: u32, pc_pos_y: u32, features_count: u32, features_idx: usize, feature: u32) -> bool {
    for i in 0..features_count {
        let feat_id = save.u32_from_save(features_idx+(i * FEATU_SIZE_IN_SAVE + 2) as usize);

        if feat_id == feature { //found statue at different position
            save.update_u8_in_save(pc_pos_x as u8, features_idx+(i * FEATU_SIZE_IN_SAVE) as usize);
            save.update_u8_in_save(pc_pos_y as u8, features_idx+(i * FEATU_SIZE_IN_SAVE + 1) as usize);
            return true;
        }
    }
    return false;
}

fn add_feature_at(save: &mut Save, pc_pos_x: u32, pc_pos_y: u32, features_count: u32,
                  features_idx: usize, features_count_idx: usize, feature: u32) -> bool {
    for i in 0..features_count {
        let feat_x = save.u8_from_save(features_idx+(i * FEATU_SIZE_IN_SAVE) as usize) as u32;
        let feat_y = save.u8_from_save(features_idx+(i * FEATU_SIZE_IN_SAVE + 1) as usize) as u32;
        if feat_x == pc_pos_x && feat_y == pc_pos_y {
            return false;
        }
    }

    //update level map
    let mut bin_feature:Vec<u8> = Vec::new();
    bin_feature.push(pc_pos_x as u8);
    bin_feature.push(pc_pos_y as u8);
    bin_feature.push(feature as u8);
    for _ in 0..3 {
        bin_feature.push(0);
    }

    save.insert_at(features_idx, bin_feature);
    save.update_u32_in_save(features_count + 1, features_count_idx);

    return true;
}