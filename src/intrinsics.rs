use crate::save::Save;

const INTRINSICS_OFFSET: usize = 4759394;

const INTRINSICS: [&str; 27] = [
    "fire resistance",
    "poison resistance",
    "cold resistance",
    "acid resistance",
    "lucky",
    "fate smile",
    "bad luck",
    "sleep resistance",
    "petrify resistance",
    "doomed",
    "random teleportation",
    "invisibility",
    "teleportation control",
    "stun resistance",
    "deathray resistance",
    "paralization resistance",
    "shock resistance",
    "see invisible",
    "fire immunity",
    "acid immunity",
    "cold immunity",
    "shock immunity",
    "water breathing",
    "enemy of undead",
    "confusion resistance",
    "disease resistance",
    "disease immunity"
];

pub(crate) fn print_intrinsic(save: &Save) {
    let intrinsics_flags = save.u32_from_save(INTRINSICS_OFFSET);

    println!("Intrinsics present:");
    print_intrinsics(intrinsics_flags);
    println!("Intrinsics absent:");
    print_intrinsics(!intrinsics_flags);
}

fn print_intrinsics(intrinsics_flags: u32) {

    if intrinsics_flags == 0 {
        println!("  None");
        return;
    }

    for x in 0..INTRINSICS.len() {
        if (intrinsics_flags & (1 << x)) != 0 {
            println!("{x}  {}", INTRINSICS[x]);
        }
    }
}

pub(crate) fn set_intrinsic(save: &mut Save, intrinsic_id: &str, set: bool) {

    let intrinsic_idx = parse_intrinsic_id(intrinsic_id);
    if intrinsic_idx >= INTRINSICS.len() {
        println!("Couldn't find '{intrinsic_id}'. Use correct id from 0 to {0} or intrinsic name.", INTRINSICS.len()-1);
        return;
    }

    let mut intrinsics_flags = save.u32_from_save(INTRINSICS_OFFSET);
    if set {
        intrinsics_flags |= 1 << intrinsic_idx;
    } else {
        intrinsics_flags &= !(1 << intrinsic_idx);
    }
    save.update_u32_in_save(intrinsics_flags, INTRINSICS_OFFSET);
}

fn parse_intrinsic_id(name_or_number: &str) -> usize {
    let intrinsic = match name_or_number.parse() {
        Ok(number) => number,
        Err(_) => INTRINSICS.iter().position(|&r| r == name_or_number.to_lowercase()).unwrap_or_else(||usize::MAX),
    };
    intrinsic
}