use crate::save::Save;
use crate::save::{HDR_SIZE, GSPD};

const SPELL_NAMES: [&str; 47] = [
    "Magic Missile",
    "Lightning Bolt",
    "Identify",
    "Light",
    "Darkness",
    "Cure Light Wounds",
    "Knock",
    "Disarm Trap",
    "Fireball",
    "Bless",
    "Slow Monster",
    "Calm Monster",
    "Cure Serious Wounds",
    "Cure Critical Wounds",
    "Heal",
    "Cure Disease",
    "Neutralize Poison",
    "Invisibility",
    "Destroy Undead",
    "Slow Poison",
    "Teleportation",
    "Remove Curse",
    "Greater Identify",
    "Earthquake",
    "Revelation",
    "Know Alignment",
    "Create Item",
    "Summon Monsters",
    "Petrification",
    "Frost Bolt",
    "Acid Bolt",
    "Fire Bolt",
    "Strength Of Atlas",
    "Magic Lock",
    "Improved Fireball",
    "Farsight",
    "Web",
    "Stun Ray",
    "Death Ray",
    "Magic Map",
    "Mystic Shovel",
    "Burning Hands",
    "Wish",
    "Scare Monster",
    "Ice Ball",
    "Lightning Ball",
    "Acid Ball"
];
const SPELL_SIZE: usize = 18;
const ID_POS: usize = 4;
const KNOW_POINTS_POS: usize = ID_POS + 4;
const LEVEL_POS: usize = KNOW_POINTS_POS + 4;
const TO_NEXT_LVL_POS: usize = LEVEL_POS + 2;

pub fn print_spell_stats(save: &Save, print_spell_stats: bool) {
    if print_spell_stats {
        let spells = save.get_section_offset(GSPD, 0);

        for i in 0..SPELL_NAMES.len() {
            let knowledge_points = save.u32_from_save(spells + HDR_SIZE + 4 + i * SPELL_SIZE + KNOW_POINTS_POS);
            let level = save.u16_from_save(spells + HDR_SIZE + 4 + i * SPELL_SIZE + LEVEL_POS);
            let to_next_level = save.u16_from_save(spells + HDR_SIZE + 4 + i * SPELL_SIZE + TO_NEXT_LVL_POS);
            println!("Id: {: >2}|{: <21} | knowledge: {: >5} | level: {: >2} | hits to next level: {: >4} |", i, SPELL_NAMES[i], knowledge_points, level, to_next_level);
        }
    }
}

pub(crate) fn set_spell_knowledge(save: &mut Save, spell: &Option<String>, new_knowledge: Option<u32>) {
    if let (Some(id), Some(new_knowledge)) = (spell, new_knowledge) {
        let spells = save.get_section_offset(GSPD, 0);
        let spell_id = parse_spell_id(id);
        if spell_id >= SPELL_NAMES.len() {
            println!("Couldn't find '{id}'. Use correct id from 0 to {0} or full spell name.", SPELL_NAMES.len()-1);
            return;
        }

        save.update_u32_in_save(new_knowledge, spells + HDR_SIZE + 4 + spell_id * SPELL_SIZE + KNOW_POINTS_POS);
    }
}

pub(crate) fn set_spell_level(save: &mut Save, spell: &Option<String>, new_proficiency: Option<u16>) {
    if let (Some(id), Some(new_proficiency)) = (spell, new_proficiency) {
        let spells = save.get_section_offset(GSPD, 0);
        let spell_id = parse_spell_id(id);
        if spell_id >= SPELL_NAMES.len() {
            println!("Couldn't find '{id}'. Use correct id from 0 to {0} or full spell name.", SPELL_NAMES.len()-1);
            return;
        }

        save.update_u16_in_save(new_proficiency, spells + HDR_SIZE + 4 + spell_id * SPELL_SIZE + LEVEL_POS);
    }
}

fn parse_spell_id(name_or_number: &str) -> usize {
    let spell_id = match name_or_number.parse() {
        Ok(number) => number,
        Err(_) => SPELL_NAMES.iter().position(|&r| r == name_or_number).unwrap_or_else(||usize::MAX),
    };
    spell_id
}
