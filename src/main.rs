use clap::Parser;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let args = savadomer::Conf::parse();
    savadomer::process_save(args)?;
    Ok(())
}
