use std::error::Error;
use clap::{Parser, Args};
use save::Save;

mod monster_mem;
mod base_attr;
mod checksums;
mod weapon_skills;
mod spells;
mod save;
mod level_features;
mod items;
mod intrinsics;

#[derive(Parser)]
pub struct Conf {
    /// Path to ADOM save file
    path: std::path::PathBuf,
    /// Set strength to new value
    #[arg(long = "st", value_parser = clap::value_parser!(u8).range(1..100))]
    strength: Option<u8>,
    /// Set learning to new value
    #[arg(long = "le", value_parser = clap::value_parser!(u8).range(1..100))]
    learning: Option<u8>,
    /// Set willpower to new value
    #[arg(long = "wi", value_parser = clap::value_parser!(u8).range(1..100))]
    willpower: Option<u8>,
    /// Set dexterity to new value
    #[arg(long = "dx", value_parser = clap::value_parser!(u8).range(1..100))]
    dexterity: Option<u8>,
    /// Set toughness to new value
    #[arg(long = "to", value_parser = clap::value_parser!(u8).range(1..100))]
    toughness: Option<u8>,
    /// Set charisma to new value
    #[arg(long = "ch", value_parser = clap::value_parser!(u8).range(1..100))]
    charisma: Option<u8>,
    /// Set appearance to new value
    #[arg(long = "ap", value_parser = clap::value_parser!(u8).range(1..100))]
    appearance: Option<u8>,
    /// Set mana to new value
    #[arg(long = "ma", value_parser = clap::value_parser!(u8).range(1..100))]
    mana: Option<u8>,
    /// Set perception to new value
    #[arg(long = "pe", value_parser = clap::value_parser!(u8).range(1..100))]
    perception: Option<u8>,

    /// Set base speed to new value
    #[arg(long = "set-base-speed")]
    base_speed: Option<u32>,
    /// Print base speed information
    #[arg(long = "base-speed")]
    print_base_speed: bool,

    /// Set base carrying capacity
    #[arg(long = "set-base-carrying")]
    base_carrying_capacity: Option<u32>,
    /// Print base carrying capacity
    #[arg(long = "base-carrying")]
    print_base_carrying_capacity: bool,

    /// Set base hit points
    #[arg(long = "set-base-hp")]
    base_hp: Option<u32>,
    /// Print base hit points
    #[arg(long = "base-hp")]
    print_base_hp: bool,

    /// Set base power points
    #[arg(long = "set-base-pp")]
    base_pp: Option<u32>,
    /// Print base power points
    #[arg(long = "base-pp")]
    print_base_pp: bool,

    /// Set alignment stones
    #[arg(long = "set-alignment-stones", allow_hyphen_values = true, value_parser = clap::value_parser!(i32).range(-10000..6000))]
    alignment_stones: Option<i32>,
    /// Print alignment stones
    #[arg(long = "alignment-stones")]
    print_alignment_stones: bool,


    /// Set gods piety
    #[arg(long = "set-piety")]
    set_piety: Option<i64>,
    /// Print gods piety
    #[arg(long = "piety")]
    print_piety:bool,

    /// Print information about first kill
    #[arg(long = "first-kill")]
    print_first_kill: bool,
    /// Print info about character kills: monster id, name and kill count
    #[arg(long = "all-kills")]
    print_all_kills: bool,

    #[command(flatten)]
    first_kill_mod: FirstKillCfg,

    ///removes dwarves from kill log
    #[arg(long = "unkill-dwarves")]
    unkill_dwarves: bool,

    ///sets cat kill counter to 0
    #[arg(long = "unkill-cats")]
    unkill_cats: bool,

    /// Print information about current PC weapon skills
    #[arg(long = "weapon-stats")]
    print_weapon_stats: bool,
    /// Name or id of weapon skill
    #[arg(long = "weapon-skill")]
    weapon_skill: Option<String>,
    /// New hits value for selected weapon skill
    #[arg(requires="weapon_skill", long = "set-weapon-level", value_parser = clap::value_parser!(u32).range(1..16))]
    weapon_new_level: Option<u32>,
    /// New level of selected weapon skill
    #[arg(requires="weapon_skill", long = "set-weapon-hits")]
    weapon_new_hits: Option<u32>,

    /// Print information about spells
    #[arg(long = "spell-stats")]
    print_spell_stats: bool,
    /// Name or id of a spell
    #[arg(long = "spell")]
    spell: Option<String>,
    /// Set spell knowledge level
    #[arg(requires="spell", long = "set-spell-knowledge")]
    spell_new_knowledge: Option<u32>,
    /// Set spell proficiency
    #[arg(requires="spell", long = "set-spell-level")]
    spell_new_proficiency: Option<u16>,

    #[command(flatten)]
    features: FeaturesCfg,

    /// Print crowning information
    #[arg(long = "crowning")]
    print_crowning: bool,

    /// Print items "popularity"
    #[arg(long = "items-popularity", value_parser = clap::value_parser!(u16).range(1..51))]
    print_items_popularity: Option<u16>,

    /// Item name or id
    #[arg(long = "item")]
    item_id: Option<String>,

    #[command(flatten)]
    item_options: ItemCfg,

    /// Print current level information
    #[arg(long = "current-level")]
    print_dungeon_level_info: bool,

    /// Print all dungeons levels information
    #[arg(long = "all-levels")]
    print_all_levels: bool,

    /// Print all levels that could be modified to hold vault
    #[arg(long = "new-vault-levels")]
    print_new_vault_levels: bool,

    /// Tries to create greater vault in selected level
    #[arg(long = "create-big-vault", value_parser = clap::value_parser!(u16).range(1..551))]
    create_greater_vault: Option<u16>,

    /// Tries to create small vault in selected level
    #[arg(long = "create-small-vault", value_parser = clap::value_parser!(u16).range(1..551))]
    create_small_vault: Option<u16>,

    /// Unlocks Bug Temple
    #[arg(long = "unlock-bug-temple")]
    unlock_bug_temple: bool,

    /// Print intrinsics information
    #[arg(long = "intrinsics")]
    print_intrinsics: bool,

    /// Adds intrinsic to PC
    #[arg(long = "add-intrinsic")]
    intrinsic_to_add: Option<String>,

    /// Removes intrinsic from PC
    #[arg(long = "del-intrinsic")]
    intrinsic_to_del: Option<String>,

    /// Searches for specific item
    #[arg(long = "find-item")]
    find_item: Option<String>,
}

#[derive(Args)]
#[group(required = false, multiple = true, requires = "item_id")]
struct ItemCfg {
    /// Modifies selected item popularity
    #[arg(long = "set-popularity")]
    new_popularity: Option<u32>,

    /// Print selected item popularity and probability for selected EDL
    #[arg(long = "popularity-at", value_parser = clap::value_parser!(u16).range(1..51))]
    item_edl: Option<u16>,
}

#[derive(Args)]
#[group(required = false, multiple = false)]
struct FirstKillCfg {
    /// Changes first kill, provide name or id
    #[arg(long = "set-first-kill")]
    first_kill_id: Option<String>,

    /// Changes first kill count to given value
    #[arg(long = "set-first-kill-count")]
    first_kill_count: Option<u32>,
}

#[derive(Args)]
#[group(required = false, multiple = false)]
struct FeaturesCfg {
    /// Create statue at current PC position
    #[arg(long = "create-statue")]
    statue_type: Option<u16>,

    /// Create forge at current PC position
    #[arg(long = "create-forge")]
    create_forge: bool,

    /// Create pool at current PC position
    #[arg(long = "create-pool")]
    create_pool: bool,

    /// Create altar at current PC position
    #[arg(long = "create-altar")]
    altar_alignment: Option<String>
}

pub fn process_save(args: Conf) -> Result<(), Box<dyn Error>> {
    let mut save = Save::new(&args.path)?;

    modify_save(&mut save, &args);

    monster_mem::print_first_kill(&save, args.print_first_kill);
    monster_mem::print_all_kills(&save, args.print_all_kills);
    weapon_skills::print_weapon_stats(&save, args.print_weapon_stats);
    spells::print_spell_stats(&save, args.print_spell_stats);
    base_attr::print_base_speed(&save, args.print_base_speed);
    base_attr::print_base_carrying_capacity(&save, args.print_base_carrying_capacity);
    base_attr::print_base_hp(&save, args.print_base_hp);
    base_attr::print_base_pp(&save, args.print_base_pp);
    base_attr::print_alignment_stones(&save, args.print_alignment_stones);
    base_attr::print_piety(&save, args.print_piety);

    if args.print_crowning {
        base_attr::print_crowning(&save);
    }

    if let Some(edl) = args.print_items_popularity {
        items::print_items_popularity(&save, edl);
    }

    if let Some(edl) = args.item_options.item_edl {
        items::print_item_popularity(&save, edl, &args.item_id.unwrap());
    }

    if args.print_dungeon_level_info {
        level_features::print_dungeon_level_info(&save);
    }

    if args.print_all_levels {
        level_features::print_levels(&save);
    }

    if args.print_new_vault_levels {
        level_features::print_vault_levels(&save);
    }

    if args.print_intrinsics {
        intrinsics::print_intrinsic(&save);
    }

    if let Some(item_id) = args.find_item {
        items::print_item_locations(&save, &item_id);
    }

    save.write_save()?;

    Ok(())
}

fn modify_save(save: &mut Save, args: &Conf) {
    base_attr::modify_stat(save, args.strength, base_attr::ST_ATTRIB_POS);
    base_attr::modify_stat(save, args.learning, base_attr::LE_ATTRIB_POS);
    base_attr::modify_stat(save, args.willpower, base_attr::WI_ATTRIB_POS);
    base_attr::modify_stat(save, args.dexterity, base_attr::DX_ATTRIB_POS);
    base_attr::modify_stat(save, args.toughness, base_attr::TO_ATTRIB_POS);
    base_attr::modify_stat(save, args.charisma, base_attr::CH_ATTRIB_POS);
    base_attr::modify_stat(save, args.appearance, base_attr::AP_ATTRIB_POS);
    base_attr::modify_stat(save, args.mana, base_attr::MA_ATTRIB_POS);
    base_attr::modify_stat(save, args.perception, base_attr::PE_ATTRIB_POS);
    base_attr::modify_base_speed(save, args.base_speed);
    base_attr::modify_base_carrying_capacity(save, args.base_carrying_capacity);
    base_attr::modify_base_hp(save, args.base_hp);
    base_attr::modify_base_pp(save, args.base_pp);
    base_attr::modify_alignment_stones(save, args.alignment_stones);
    base_attr::modify_piety(save, args.set_piety);

    monster_mem::modify_first_kill_id(save, &args.first_kill_mod.first_kill_id);
    monster_mem::modify_first_kill_count(save, &args.first_kill_mod.first_kill_count);
    monster_mem::unkill_dwarves(save, args.unkill_dwarves);
    monster_mem::unkill_cats(save, args.unkill_cats);

    weapon_skills::set_weapon_level(save, &args.weapon_skill, args.weapon_new_level);
    weapon_skills::set_weapon_hits(save, &args.weapon_skill, args.weapon_new_hits);

    spells::set_spell_knowledge(save, &args.spell, args.spell_new_knowledge);
    spells::set_spell_level(save, &args.spell, args.spell_new_proficiency);

    if let Some(new_popularity) = &args.item_options.new_popularity {
        items::set_item_popularity(save, args.item_id.as_ref().unwrap(), *new_popularity);
    }

    level_features::create_statue(save, args.features.statue_type);
    if args.features.create_forge {
        level_features::create_forge(save);
    }
    if args.features.create_pool {
        level_features::create_pool(save);
    }

    if let Some(altar_alignment) = &args.features.altar_alignment {
        level_features::create_altar(save, altar_alignment);
    }

    if let Some(level_to_change) = &args.create_greater_vault {
        level_features::create_vault(save, *level_to_change, level_features::BIG_VAULT);
    }

    if let Some(level_to_change) = &args.create_small_vault {
        level_features::create_vault(save, *level_to_change, level_features::SMALL_VAULT);
    }

    if args.unlock_bug_temple {
        level_features::unlock_bug_temple(save);
    }

    if let Some(intrinsic_to_add) = &args.intrinsic_to_add {
        intrinsics::set_intrinsic(save, intrinsic_to_add, true);
    }

    if let Some(intrinsic_to_del) = &args.intrinsic_to_del {
        intrinsics::set_intrinsic(save, intrinsic_to_del, false);
    }
}
