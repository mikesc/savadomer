use std::collections::HashMap;
use std::error::Error;

pub const HDR_SIZE: usize = 8; //4 letters + 4 bytes for version
const EXPECTED_LVNF_COUNT: usize = 5100;
const LVNF_SIZE: usize = 125;
const ADOM:&str = "ADOM";
const ADOM_VER:u32 = 2;
const PLYR:&str = "PLYR";
const PLYR_VER:u32 = 0x1e;
pub const LVNF:&str = "LVNF";
const LVNF_VER:u32 = 1;
const GMCP:&str = "GMCP";
const GMCP_VER:u32 = 2;
pub(crate) const GDSD:&str = "GDSD";
const GDSD_VER:u32 = 1;
const GEVT:&str = "GEVT";
const GEVT_VER:u32 = 1;
pub const GIVD:&str = "GIVD";
const GIVD_VER:u32 = 1;
pub const GSPD:&str = "GSPD";
const GSPD_VER:u32 = 1;
pub(crate) const GITD:&str = "GITD";
const GITD_VER:u32 = 1;
const GSKD:&str = "GSKD";
const GSKD_VER:u32 = 1;
pub const GMTP:&str = "GMTP";
const GMTP_VER:u32 = 1;
pub const GMST:&str = "GMST";
const GMST_VER:u32 = 1;
const GBSS:&str = "GBSS";
const GBSS_VER:u32 = 1;
const GMAS:&str = "GMAS";
const GMAS_VER:u32 = 1;
const AMMY:&str = "AMMY";
const AMMY_VER:u32 = 1;
const GCHD:&str = "GCHD";
const GCHD_VER:u32 = 1;
const GRLG:&str = "GRLG";
const GRLG_VER:u32 = 1;
const GWPS:&str = "GWPS";
const GWPS_VER:u32 = 1;
pub const WPNS:&str = "WPNS";
const WPNS_VER:u32 = 1;
const EXPECTED_WPNS_COUNT: usize = 19; //10 h2h + 8 rng + 1 shd
const GHNT:&str = "GHNT";
const GHNT_VER:u32 = 1;
const ALVL:&str = "ALVL";
const ALVL_VER:u32 = 1;
pub const LVMP:&str = "LVMP";
const LVMP_VER:u32 = 2;

pub(crate) const LITS:&str = "LITS";
const LITS_VER:u32 = 1;
pub(crate) const LMST:&str = "LMST";
const LMST_VER:u32 = 1;
pub(crate) const MAIV:&str = "MAIV";
const MAIV_VER:u32 = 1;

struct ChecksumRange {
    start: usize,
    end: usize,
    xor_val: u32,
}
const CHECKSUM_SIZE: usize = 20; //checksum and its 4 derivatives take 20 bytes
pub(crate) const LVL_VISITED_FLAGS_POS: usize = 4080045;
pub const PC_DNG_POS: usize = 41;
pub const PC_DNG_LVL_POS: usize = 43;
pub(crate) const PC_POS_X_IDX: usize = 4121094;
pub(crate) const PC_POS_Y_IDX: usize = 4121098;
const LVL_PER_DNG: usize = 100;
pub(crate) struct Save<'a> {
    path: &'a std::path::PathBuf,
    data: Vec<u8>,
    section_idx:HashMap<&'static str, Vec<usize>>,
    checksum_positions: Vec<ChecksumRange>,
    pub(crate) lvldng_to_idx: HashMap<(u16, u16), usize>,
}

impl Save<'_> {
    pub(crate) fn new(path: &std::path::PathBuf) -> Result<Save, Box<dyn Error>> {
        let mut ret = Save {
            path,
            data: std::fs::read(path)?,
            section_idx: HashMap::new(),
            checksum_positions: Vec::new(),
            lvldng_to_idx: HashMap::new(),
        };

        ret.extract_sections()?;
        ret.extract_checksums()?;

        Ok(ret)
    }

    pub(crate) fn write_save(&mut self) -> Result<(), Box<dyn Error>> {
        let (checksums, bad_count) = self.compute();
        if bad_count == 0 {
            println!("All {} checksums are OK!", self.checksum_positions.len());
        } else {
            println!("{bad_count} of {} checksums are bad.", self.checksum_positions.len());

            println!("Fixing them...");
            self.fix_in_file(&checksums);
            std::fs::write(self.path, &self.data)?;
            println!("Done");
        }
        Ok(())
    }

    pub(crate) fn u64_from_save(&self, pos: usize) -> u64 {
        u64::from(self.data[pos])
            + (u64::from(self.data[pos + 1]) << 8)
            + (u64::from(self.data[pos + 2]) << 16)
            + (u64::from(self.data[pos + 3]) << 24)
            + (u64::from(self.data[pos + 4]) << 32)
            + (u64::from(self.data[pos + 5]) << 40)
            + (u64::from(self.data[pos + 6]) << 48)
            + (u64::from(self.data[pos + 7]) << 56)
    }

    pub(crate) fn update_u64_in_save(&mut self, val: u64, pos: usize) {
        self.data[pos] = (val & 0xFF) as u8;
        self.data[pos + 1] = ((val >> 8) & 0xFF) as u8;
        self.data[pos + 2] = ((val >> 16) & 0xFF) as u8;
        self.data[pos + 3] = ((val >> 24) & 0xFF) as u8;
        self.data[pos + 4] = ((val >> 32) & 0xFF) as u8;
        self.data[pos + 5] = ((val >> 40) & 0xFF) as u8;
        self.data[pos + 6] = ((val >> 48) & 0xFF) as u8;
        self.data[pos + 7] = ((val >> 56) & 0xFF) as u8;
    }

    pub(crate) fn u32_from_save(&self, pos: usize) -> u32 {
        u32::from(self.data[pos])
            + (u32::from(self.data[pos + 1]) << 8)
            + (u32::from(self.data[pos + 2]) << 16)
            + (u32::from(self.data[pos + 3]) << 24)
    }

    pub(crate) fn u16_from_save(&self, pos: usize) -> u16 {
        u16::from(self.data[pos]) + (u16::from(self.data[pos + 1]) << 8)
    }

    pub(crate) fn update_u32_in_save(&mut self, val: u32, pos: usize) {
        self.data[pos] = (val & 0xFF) as u8;
        self.data[pos + 1] = ((val >> 8) & 0xFF) as u8;
        self.data[pos + 2] = ((val >> 16) & 0xFF) as u8;
        self.data[pos + 3] = ((val >> 24) & 0xFF) as u8;
    }

    pub(crate) fn update_u16_in_save(&mut self, val: u16, pos: usize) {
        self.data[pos] = (val & 0xFF) as u8;
        self.data[pos + 1] = ((val >> 8) & 0xFF) as u8;
    }

    pub(crate) fn u8_from_save(&self, pos: usize) -> u8 {
        self.data[pos]
    }

    pub(crate) fn update_u8_in_save(&mut self, val: u8, pos: usize) {
        self.data[pos] = val;
    }

    pub(crate) fn insert_at(&mut self, pos: usize, data: Vec<u8>) {
        // TODO do it in one go
        for i in 0..data.len() {
            self.data.insert(pos + i, data[i]);
        }

        for checksum in &mut self.checksum_positions {
            if checksum.start >= pos {
                checksum.start += data.len();
            }
            if checksum.end >= pos {
                checksum.end += data.len();
            }
        }

        for section_class in &mut self.section_idx {
            for section in section_class.1 {
                if *section >= pos {
                    *section += data.len();
                }
            }
        }
    }

    pub(crate) fn get_sections_of_type(&self, section_name: &str) -> &Vec<usize> {
        self.section_idx.get(section_name).unwrap()
    }

    pub(crate) fn get_section_offset(&self, section_name: &str, section_index: usize) -> usize {
        *self.section_idx.get(section_name).unwrap().get(section_index).unwrap()
    }

    // returns index of LVMP entry with matching lvl/dng
    pub(crate) fn get_lvl_dng_lvmp_index(&self, lvl: u16, dng: u16) -> usize {
        *self.lvldng_to_idx.get_key_value(&(lvl, dng)).unwrap().1
    }

    // returns offset to lvnf with matching lvl/dng
    pub(crate) fn get_lvl_dng_lvnf_offset(&self, lvl: u16, dng: u16) -> usize {
        *self.section_idx.get(LVNF).unwrap().get(dng as usize * LVL_PER_DNG + lvl as usize).unwrap()
    }
}

impl Save<'_> {
    fn extract_checksums(&mut self) -> Result<(), Box<dyn Error>> {
        self.checksum_positions.push(ChecksumRange {
            start: 0,
            end: self.section_idx.get(GIVD).unwrap().first().unwrap() - CHECKSUM_SIZE,
            xor_val: 0xaef0ffa0
        });

        self.checksum_positions.push(ChecksumRange {
            start: *self.section_idx.get(GIVD).unwrap().first().unwrap(),
            end: self.section_idx.get(GMTP).unwrap().first().unwrap() - CHECKSUM_SIZE,
            xor_val: 0x12345678
        });

        self.get_level_checksums()?;

        self.checksum_positions.push(ChecksumRange {
            start: self.data.len() - CHECKSUM_SIZE * 2,
            end: self.data.len() - CHECKSUM_SIZE * 2,
            xor_val: 0x05afc241
        });
        self.checksum_positions.push(ChecksumRange {
            start: self.data.len() - CHECKSUM_SIZE,
            end: self.data.len() - CHECKSUM_SIZE,
            xor_val: 0x0
        });
        Ok(())
    }


    fn get_level_checksums(&mut self) -> Result<(), Box<dyn Error>> {
        let level_info_start_pos = self.section_idx.get(LVNF).unwrap();
        let level_map_start_pos = self.section_idx.get(LVMP).unwrap();
        let mut fst_lvl_found = false;
        let mut lvl_checksum_start_pos = *self.section_idx.get(GMTP).unwrap().first().unwrap();
        let mut lvmp_idx = 0;
        let mut xor_val = 0;
        let pc_lvl = self.u16_from_save(PC_DNG_LVL_POS);
        let pc_dng = self.u16_from_save(PC_DNG_POS);
        for dng in 1..51 {
            for lvl in 0..100 {
                let lvl_visited = self.u32_from_save(LVL_VISITED_FLAGS_POS + (400 * dng) + (lvl * 4));
                let lvl_type = self.u32_from_save(level_info_start_pos[dng * 100 + lvl] + HDR_SIZE) as i32;

                if lvl_visited != 0 ||
                    ((lvl_type == 0x41 || lvl_type == 0x42 || lvl_type == 0x80) && (dng == pc_dng as usize) && (lvl == pc_lvl as usize)) {
                    self.lvldng_to_idx.insert((lvl as u16, dng as u16), lvmp_idx);
                    let pos = level_map_start_pos[lvmp_idx];
                    lvmp_idx += 1;
                    if fst_lvl_found {
                        self.checksum_positions.push(ChecksumRange {
                            start: lvl_checksum_start_pos,
                            end: pos - CHECKSUM_SIZE,
                            xor_val
                        });
                        lvl_checksum_start_pos = pos;
                    }
                    xor_val = (lvl_type * ((dng * lvl) as i32)) as u32;
                    fst_lvl_found = true;
                }
            }
        }
        //last level checksum
        self.checksum_positions.push(ChecksumRange {
            start: lvl_checksum_start_pos,
            end: self.data.len() - CHECKSUM_SIZE * 3,
            xor_val
        });
        if lvmp_idx != level_map_start_pos.len() {
            return Err(format!("There are {} level checksums but {} found. Please contact project through gitlab page",
                               level_map_start_pos.len(), lvmp_idx).into())
        }
        Ok(())
    }

    fn compute(&self) -> (Vec<u32>, u32) {
        let mut checksum: u32 = 0xffffffff;
        let mut checksums: Vec<u32> = Vec::with_capacity(self.checksum_positions.len());
        let mut bad_count = 0;

        for (idx, range) in self.checksum_positions.iter().enumerate() {
            for i in range.start..range.end {
                let b = self.data[i];
                let idx = ((u32::from(b) ^ checksum) & 0xff) as usize;
                let magic_val = crate::checksums::MAGIC_TAB[idx];
                checksum = (checksum >> 8) ^ magic_val;
            }

            checksum ^= range.xor_val;

            // flip last checksum
            if idx + 1 == self.checksum_positions.len() {
                checksum = !checksum;
            }

            let save_local_checksum: u32 = self.u32_from_save(range.end + 16);

            if save_local_checksum != checksum {
                bad_count += 1;
            }
            checksums.push(checksum);

            checksum ^= range.xor_val;
        }
        (checksums, bad_count)
    }

    fn fix_in_file(&mut self, checksums: &Vec<u32>) {
        for (idx, checksum) in checksums.iter().enumerate() {
            let mangle1 = checksum / 50;
            let mangle2 = mangle1.wrapping_sub(*checksum);
            let mangle3 = mangle1.wrapping_mul(mangle2);
            let mangle4 = mangle3 ^ mangle2;
            let pos = self.checksum_positions[idx].end;

            self.update_u32_in_save(mangle1, pos);
            self.update_u32_in_save(mangle2, pos + 4);
            self.update_u32_in_save(mangle3, pos + 8);
            self.update_u32_in_save(mangle4, pos + 12);
            self.update_u32_in_save(*checksum, pos + 16);
        }
    }

    fn extract_sections(&mut self) -> Result<(), Box<dyn Error>> {
        let mut section_start = self.extract_section(ADOM, ADOM_VER, 0)?;
        section_start = self.extract_section(PLYR, PLYR_VER, section_start+HDR_SIZE)?;
        for _ in 0..EXPECTED_LVNF_COUNT {
            section_start = self.extract_section(LVNF, LVNF_VER, section_start + LVNF_SIZE)?;
        }
        section_start = self.extract_section(GMCP, GMCP_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GDSD, GDSD_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GEVT, GEVT_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GIVD, GIVD_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GSPD, GSPD_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GITD, GITD_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GSKD, GSKD_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GMTP, GMTP_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GMST, GMST_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GBSS, GBSS_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GMAS, GMAS_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(AMMY, AMMY_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GCHD, GCHD_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GRLG, GRLG_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(GWPS, GWPS_VER, section_start+HDR_SIZE)?;
        for _ in 0..EXPECTED_WPNS_COUNT {
            section_start = self.extract_section(WPNS, WPNS_VER, section_start+HDR_SIZE)?;
        }
        section_start = self.extract_section(GHNT, GHNT_VER, section_start+HDR_SIZE)?;
        section_start = self.extract_section(ALVL, ALVL_VER, section_start+HDR_SIZE)?;
        while let Ok(s) = self.extract_section(LVMP, LVMP_VER, section_start+HDR_SIZE) {
            section_start = s;
            section_start = self.extract_section(LITS, LITS_VER, section_start+HDR_SIZE)?;
            section_start = self.extract_section(LMST, LMST_VER, section_start+HDR_SIZE)?;
            section_start = self.extract_section(MAIV, MAIV_VER, section_start+HDR_SIZE)?;
        }
        Ok(())
    }

    fn extract_section(&mut self, name: &'static str, version: u32, start_pos: usize) -> Result<usize, Box<dyn Error>> {
        let section_start = self.find_section(name, version, start_pos)?;
        self.section_idx.entry(name).or_default().push(section_start);
        Ok(section_start)
    }

    pub(crate) fn find_section(&self, txt: &'static str, ver: u32, start_pos: usize) -> Result<usize, Box<dyn Error>> {
        let mut needle:Vec<u8> = Vec::new();
        needle.extend_from_slice(txt.as_bytes());
        needle.extend_from_slice(&ver.to_le_bytes());
        match self.data[start_pos..].windows(needle.len()).position(|window| window == needle) {
            Some(pos) => Ok(pos + start_pos),
            None => Err(format!("couldn't find block '{txt}' with version number {ver} from position {start_pos}").into())
        }
    }
}