use crate::save::Save;
use crate::save::{WPNS, HDR_SIZE};

const WPN_CATEGORY_NAMES: [&str; 19] = [
    "Unarmed fighting",
    "Daggers & knives",
    "Clubs & hammers",
    "Maces & flails",
    "Swords",
    "Axes",
    "Whips",
    "Pole arms",
    "Twohanded weapons",
    "Staves",
    "Slings",
    "Bows",
    "Crossbows",
    "Thrown axes & hammers",
    "Thrown daggers",
    "Thrown rocks & clubs",
    "Thrown spears",
    "Boomerangs & scurgari",
    "Shields"
];

pub(crate) fn print_weapon_stats(save: &Save, print_weapon_stats: bool) {
    if print_weapon_stats {
        let weapons = save.get_sections_of_type(WPNS);

        for i in 0..weapons.len() {
            let level = save.u32_from_save(weapons[i] + HDR_SIZE);
            let total_hits = save.u32_from_save(weapons[i] + HDR_SIZE + 4);
            println!("Id: {: >2}|{: <21} | level: {: >2} | current hits: {: >5} |", i, WPN_CATEGORY_NAMES[i], level, total_hits);
        }
        println!("\nTo advance |level, required hits|:");
        for i in 0..weapons.len() {
            let mut s = String::new();
            for j in 2..16 {
                let req_hits = save.u32_from_save(weapons[i] + HDR_SIZE + 8 + 4 * j);
                s.push_str(&format!("|{}, {}", j, req_hits));
            }
            println!("{: <21} {}", WPN_CATEGORY_NAMES[i], s);
        }
    }
}

pub(crate) fn set_weapon_level(save: &mut Save, weapon_skill: &Option<String>, new_level: Option<u32>) {
    if let (Some(id), Some(new_level)) = (weapon_skill,new_level) {
        let weapons = save.get_sections_of_type(WPNS);
        let weapon_skill_id = parse_weapon_skill_id(id);
        if weapon_skill_id >= WPN_CATEGORY_NAMES.len() {
            println!("Couldn't find '{id}'. Use correct id from 0 to {0} or full weapon skill name.", WPN_CATEGORY_NAMES.len()-1);
            return;
        }
        save.update_u32_in_save(new_level, weapons[weapon_skill_id] + HDR_SIZE);
    }
}

pub(crate) fn set_weapon_hits(save: &mut Save, weapon_skill: &Option<String>, new_hits: Option<u32>) {
    if let (Some(id), Some(new_hits)) = (weapon_skill,new_hits) {
        let weapons = save.get_sections_of_type(WPNS);
        let weapon_skill_id = parse_weapon_skill_id(id);
        if weapon_skill_id >= WPN_CATEGORY_NAMES.len() {
            println!("Couldn't find '{id}'. Use correct id from 0 to {0} or full weapon skill name.", WPN_CATEGORY_NAMES.len()-1);
            return;
        }
        save.update_u32_in_save(new_hits, weapons[weapon_skill_id] + HDR_SIZE + 4);
    }
}

pub(crate) fn parse_weapon_skill_id(name_or_number: &str) -> usize {
    let weapon_id = match name_or_number.parse() {
        Ok(number) => number,
        Err(_) => WPN_CATEGORY_NAMES.iter().position(|&r| r == name_or_number).unwrap_or_else(||usize::MAX),
    };
    weapon_id
}