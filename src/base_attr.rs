use crate::items::item_name;
use crate::save::Save;

pub(crate) const ST_ATTRIB_POS: usize = 0x3EE266;
pub(crate) const LE_ATTRIB_POS: usize = 0x3EE26A;
pub(crate) const WI_ATTRIB_POS: usize = 0x3EE26E;
pub(crate) const DX_ATTRIB_POS: usize = 0x3EE272;
pub(crate) const TO_ATTRIB_POS: usize = 0x3EE276;
pub(crate) const CH_ATTRIB_POS: usize = 0x3EE27A;
pub(crate) const AP_ATTRIB_POS: usize = 0x3EE27E;
pub(crate) const MA_ATTRIB_POS: usize = 0x3EE282;
pub(crate) const PE_ATTRIB_POS: usize = 0x3EE286;
const POTENTIAL_ATTRIB_OFFSET: usize = 9*4;
const MOD_HIST_ATTRIB_OFFSET: usize = 9*4*2;
const BASE_SPEED_OFFSET: usize = 4760165;
const BASE_CARRYING_CAPACITY_OFFSET: usize = 4759358;
const BASE_HP_MAX_OFFSET: usize = 4120864;
const BASE_PP_MAX_OFFSET: usize = 4759354;
const ALIGNMENT_STONES_OFFSET: usize = 4759466;

const PIETY_OFFSET: usize = 4759470;
const STARTING_PIETY_OFFSET: usize = 4759494;
const CROWNING_GIFT_OFFSET: usize = 4759914;
const IS_CROWNED_OFFSET: usize = 4759918;
const IS_FALLEN_CHAMPION: usize = 4759922;


pub(crate) fn modify_stat(save: &mut Save, new_attribute_val: Option<u8>, offset: usize) {
    if let Some(attr) = new_attribute_val {
        let old_attrib = save.u32_from_save(offset) as u8;
        if old_attrib != attr {
            save.update_u32_in_save(attr as u32, offset);

            //increase potential attribute vale if needed
            let old_potential_attr = save.u32_from_save(offset + POTENTIAL_ATTRIB_OFFSET) as u8;
            if old_potential_attr < attr {
                save.update_u32_in_save(attr as u32, offset + POTENTIAL_ATTRIB_OFFSET);
            }

            //record attribute change in character history
            let mod_hist_upd = attr as i32 - old_attrib as i32;
            let mod_hist = save.u32_from_save(offset + MOD_HIST_ATTRIB_OFFSET) as i32;
            save.update_u32_in_save((mod_hist + mod_hist_upd) as u32, offset + MOD_HIST_ATTRIB_OFFSET);
        }
    }
}

pub(crate) fn modify_base_speed(save: &mut Save, new_speed_val: Option<u32>) {
    if let Some(new_speed) = new_speed_val {
        save.update_u32_in_save(new_speed, BASE_SPEED_OFFSET);
    }
}

pub(crate) fn print_base_speed(save: &Save, print_base_speed: bool) {
    if print_base_speed {
        println!("Base speed is: {}", save.u32_from_save(BASE_SPEED_OFFSET));
    }
}

pub(crate) fn modify_base_carrying_capacity(save: &mut Save, new_carrying_capacity_val: Option<u32>) {
    if let Some(new_capacity) = new_carrying_capacity_val {
        save.update_u32_in_save(new_capacity, BASE_CARRYING_CAPACITY_OFFSET);
    }
}

pub(crate) fn print_base_carrying_capacity(save: &Save, print_base_carrying_capacity: bool) {
    if print_base_carrying_capacity {
        println!("Base carrying capacity is: {}", save.u32_from_save(BASE_CARRYING_CAPACITY_OFFSET));
    }
}

pub(crate) fn modify_base_hp(save: &mut Save, new_base_hp: Option<u32>) {
    if let Some(new_hp) = new_base_hp {
        save.update_u32_in_save(new_hp, BASE_HP_MAX_OFFSET);
    }
}

pub(crate) fn print_base_hp(save: &Save, print_base_hp: bool) {
    if print_base_hp {
        println!("HP base is: {}", save.u32_from_save(BASE_HP_MAX_OFFSET));
    }
}

pub(crate) fn modify_base_pp(save: &mut Save, new_base_pp: Option<u32>) {
    if let Some(new_pp) = new_base_pp {
        save.update_u32_in_save(new_pp, BASE_PP_MAX_OFFSET);
    }
}

pub(crate) fn print_base_pp(save: &Save, print_base_pp: bool) {
    if print_base_pp {
        println!("PP base is: {}", save.u32_from_save(BASE_PP_MAX_OFFSET));
    }
}

pub(crate) fn modify_alignment_stones(save: &mut Save, new_stones: Option<i32>) {
    if let Some(new_stones) = new_stones {
        save.update_u32_in_save(new_stones as u32, ALIGNMENT_STONES_OFFSET);
    }
}

pub(crate) fn print_alignment_stones(save: &Save, print_alignment_stones: bool) {
    if print_alignment_stones {
        println!("Alignment stones value is: {}", save.u32_from_save(ALIGNMENT_STONES_OFFSET) as i32);
    }
}

pub(crate) fn modify_piety(save: &mut Save, new_piety: Option<i64>) {
    if let Some(new_piety) = new_piety {
        save.update_u64_in_save(new_piety as u64, PIETY_OFFSET);
        save.update_u64_in_save(new_piety as u64, PIETY_OFFSET + 8);
        save.update_u64_in_save(new_piety as u64, PIETY_OFFSET + 16);
    }
}

pub(crate) fn print_piety(save: &Save, print_piety: bool) {
    if print_piety {
        println!("Piety of chaotic god: {}", save.u64_from_save(PIETY_OFFSET) as i64);
        println!("Piety of neutral god: {}", save.u64_from_save(PIETY_OFFSET + 8) as i64);
        println!("Piety of lawfull god: {}", save.u64_from_save(PIETY_OFFSET + 16) as i64);

        println!("Starting piety of chaotic god: {}", save.u64_from_save(STARTING_PIETY_OFFSET) as i64);
        println!("Starting piety of neutral god: {}", save.u64_from_save(STARTING_PIETY_OFFSET + 8) as i64);
        println!("Starting piety of lawfull god: {}", save.u64_from_save(STARTING_PIETY_OFFSET + 16) as i64);
    }
}

pub(crate) fn print_crowning(save: &Save) {
    let crowning_item_id = save.u32_from_save(CROWNING_GIFT_OFFSET) as usize;
    let is_crowned_flag = save.u32_from_save(IS_CROWNED_OFFSET);
    let is_fallen_champion_flag = save.u32_from_save(IS_FALLEN_CHAMPION);

    println!("Crowning gift: {}", item_name(crowning_item_id));
    println!("Is PC crowned: {}", if is_crowned_flag != 0 {"Yes"} else {"No"});
    println!("Is PC a fallen champion: {}", if is_fallen_champion_flag != 0 {"Yes"} else {"No"});
}