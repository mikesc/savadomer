# savadomer

It's a save game editor of ADOM (Ancient Domains of Mystery).

It targets ADOM version 3.3.4, latest on Steam and GOG as of 02.07.2024.

## usage

### To display help.

```savadomer --help```

### Fix checksums

When only path to save file is provided savadomer will check and update checksums if needed.
Useful when save file was modified manually.

```savadomer <path_to_save>```

### There are nine options that can modify character attributes.

```savadomer --st 33 --ma 10 <path_to_save>```

```savadomer --st 25 --le 1 --wi 99 --dx 99 --to 99 --ch 1 --ap 99 --ma 1 --pe 99  <path_to_save>```

### Modification and information about base HP and PP values

Base hp and pp values don't include modifies that case from attributes and leveling.

```savadomer --set-base-hp 100 --set-base-pp 123``` sets base hp to 100 and pp to 123

```savadomer --base-hp --base-pp <path_to_save>``` prints information

### Modification and information about speed

```savadomer --set-base-speed 200 <path_to_save>```

```savadomer --base-speed <path_to_save>```

### Modification and information about base weight capacity

Default value is 1000.

```savadomer --set-base-carrying 2000 <path_to_save>```

```savadomer --base-carrying <path_to_save>```

### Modification and information about alignment stones

Alignment stones can't be below -10000 and above 6000.

```savadomer --set-alignment-stones=-1``` sets stones to -1 (neutral)

```savadomer --alignment-stones``` prints information

### Modification and information about piety

```savadomer --piety``` prints information about current and starting piety.

```savadomer ---set-piety 100000``` sets all gods piety to 100000.

### Getting information about first kill and all kills

```savadomer --first-kill <path_to_save>```

```savadomer --all-kills <path_to_save>```

All kill list also provides id numbers that can be used instead of monster name to change first kill.

### Modification of first kill

```savadomer --set-first-kill "goblin rockthrower" <path_to_save>```

is equivalent to

```savadomer --set-first-kill 225 <path_to_save>```

### Modification of first kill count

```savadomer --set-first-kill-count 5 <path_to_save>```

### Reset dwarves or cats kill count

```savadomer --unkill-dwarves <path_to_save>```

```savadomer --unkill-cats <path_to_save>```

### Print information about PC weapon skills

```savadomer --weapon-stats <path_to_save>```

### Modify weapon skill level

```savadomer --weapon-skill "Unarmed fighting" --set-weapon-level 11 <path_to_save>```

is equivalent to

```savadomer --weapon-skill 0 --set-weapon-level 11 <path_to_save>```

### Modify weapon skill hits

```savadomer --weapon-skill "Unarmed fighting" --set-weapon-hits 123 <path_to_save>```

is equivalent to

```savadomer --weapon-skill 0 --set-weapon-hits 123 <path_to_save>```

Setting weapon hits to value equal to or greater than required for next level prevents weapon skill leveling.

### Print information about PC spells

```savadomer --spell-stats <path_to_save>```

### Modify spell memory and/or proficiency level

```savadomer  --spell "Mystic Shovel" --set-spell-knowledge 99 --set-spell-level 9 <path_to_save>```

### Add/modify statue at current level

```savadomer --create-statue 100 <path_to_save>```

Adds or moves statue at current PC position. Statue type is set to 100. Statues types (Idx) can be taken from
this [link](https://gitlab.com/mikesc/savadomer/-/blob/master/doc/statues.md?ref_type=heads).

### Add forge/pool at current level

```savadomer --create-forge <path_to_save>```

```savadomer --create-pool <path_to_save>```

Adds forge/pool at current PC position. There can be many forges/pools on one level.

### Add/move altar at current level

```savadomer --create-altar chaotic <path_to_save>```

```savadomer --create-altar neutral <path_to_save>```

```savadomer --create-altar lawfull <path_to_save>```

Moves and modifies altar alignment or creates new one with specified alignment at PC current position.

### Print crowning information

```savadomer --crowning <path_to_save>```

Print information about crowning status and gift.

### Print information about item generation probabilities

```savadomer --items-popularity <EDL> <path_to_save>```

Prints all items popularity and generation probability.

### Print information / modify item generation probability

```savadomer --item 'ring of djinni summoning' --popularity-at 10```

Prints information about RoDS on EDL 10.

```savadomer --item 'amulet of life saving' --popularity-at 39 --set-popularity 500000```

Modifies AoLS popularity and prints updated information about AoLS generation probability on EDL 30.

### Level information and modification

```savadomer --current-level <path_to_save>```

Prints current level information (ID, type, EDL, Name).

```savadomer --all-levels <path_to_save>```

Prints information about all levels (ID, type, EDL, Name, Visited).

```savadomer --new-vault-levels <path_to_save>```

Prints information about levels that can be modified to contain small/big vault.

```savadomer --create-big-vault <level_id> <path_to_save>```

and

```savadomer --create-big-vault <level_id> <path_to_save>```

Change unvisited level into one that contains small/big vault.

### Bug temple unlocking

```savadomer --unlock-bug-temple <path_to_save>```

will unlock bug temple for save specified.

### Intrinsics

```savadomer --intrinsics <path_to_save>```

prints PC intrinsics

```savadomer --add-intrinsics <path_to_save>```

```savadomer --del-intrinsics <path_to_save>```

can be used to modify them


### Finding items

```savadomer --find-item "seven league boots" <path_to_save>```

and

```savadomer --find-item 558 <path_to_save>```

will search levels and monsters inventories for 'seven league boots'.

PC inventory, companions and monster in transit between levels are not searched.