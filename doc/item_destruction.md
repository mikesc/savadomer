## Missile destruction
```
x = 1 for glass missiles
x = 3 for non glass missiles
if missile is blessed {
  x += 4
}

if missile is arrow or bolt {
  x stays unchanged
} else if missile is sling missile {
  x += 3
} else if some kind of missile {
  x += 8
} else if some other kind { //TODO
  x += 3
} else {
  x += 1 if not glass
}

rand = R(x * 100)
threshold = missile damage chance to crushing
if missile is rusted
  threshold += 50
if rand < threshold + PC_Curse * 2 - PC_Luck * 2
  missile is destroyed
```


### Material damage chance

| ID | Material\Dmg type | Fire | Water | Crushing | Explosion | Lightning | Acid | Cold | Corruption | Stone breath |
|----|-------------------|------|-------|----------|-----------|-----------|------|------|------------|--------------|
| 0  | Iron              | 3    | 15    | 5        | 10        | 0         | 25   | 0    | 0          | 6            |
| 1  | Mithril           | 0    | 0     | 1        | 1         | 0         | 5    | 0    | 0          | 0            |
| 2  | \<Unused\>        | 5    | 0     | 1        | 5         | 5         | 8    | 0    | 0          | 2            |
| 3  | Leather           | 10   | 0     | 0        | 10        | 10        | 20   | 0    | 0          | 8            |
| 4  | Cloth             | 40   | 0     | 0        | 50        | 20        | 10   | 0    | 0          | 20           |
| 5  | Glass             | 20   | 0     | 80       | 60        | 5         | 0    | 10   | 0          | 90           |
| 6  | Paper             | 95   | 40    | 10       | 50        | 20        | 50   | 5    | 0          | 40           |
| 7  | Non metal/organic | 25   | 0     | 10       | 25        | 5         | 15   | 0    | 0          | 10           |
| 8  | Wood              | 30   | 0     | 30       | 30        | 20        | 20   | 0    | 0          | 30           |
| 9  | Gold              | 15   | 0     | 0        | 10        | 0         | 25   | 0    | 0          | 2            |
| 10 | Stone             | 2    | 0     | 5        | 8         | 3         | 0    | 1    | 0          | 6            |
| 11 | Adamantium        | 0    | 0     | 1        | 0         | 0         | 1    | 0    | 0          | 1            |
| 12 | Eternium          | 0    | 0     | 0        | 0         | 0         | 0    | 0    | 0          | 0            |
| 13 | Crystal           | 0    | 0     | 0        | 0         | 0         | 0    | 0    | 0          | 0            |
| 14 | Truesilver        | 0    | 0     | 1        | 1         | 0         | 1    | 0    | 0          | 0            |