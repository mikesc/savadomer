### Weapon Damage
```
if PC is unburdened monk with no weapon {
    dmg = 1d(PC_lvl + 8) + PC_lvl / 4
} else if PC is beastfighter with no weapon {
    dmg = 1d(PC_lvl / 3 * 2 + 4) + PC_lvl / 2
} else if PC fight with no weapon {
    if PC has Thorns chaos power {
        dmg = 3d3
    } else {
        dmg = 1d3
    }
} else {  //has weapon
    dmg = ItemMeleeDamage() //weapon dice throws
    
    if item is Rolf's Companion and PC is dwarf
        dmg2 = ItemMeleeDamage()
        dmg = MAX(dmg, dmg2)
    }
    if item is fired torch
        dmg += 1d4
}

dmg += PCMeleeDamageMods() //described below

if weapon is broken
    dmg /= 2
if weapon is rusted
    dmg /= 2

if backstabbing check passed { //there is 1 backstabbing check per attack (it is also checked for crit)
    dmg_mul = 2
    if PC is thief or assasin {
        if uses dagger
            dmg_mul = 5
        else
            dmg_mul = 2
        
        if PC_lvl is 12 or more
            dmg_mul +=2 //7 or 4 
    }
    
    dmg = dmg * dmg_mul
}

if monster is undead {
    if weapon "of sun"
        dmg *= 2
    if "ashen" weapon
        dmg = 0
    if PC is non chaotic mist elf
        dmg += MAX(4, PC_lvl / 4)  //Mist elfs OP :)
}
if tremendous blow
    dmg *= 3
else if mighty blow
    dmg *= 2

if room "filled with a deadly and chilling silence"
    dmg *= 10
if room "seems to vibrate with life"
    dmg /= 10
if monster is undead or demon {
    if weapon is blessed
        dmg = (dmg * 3) / 2
    if weapon is cursed
        dmg /= 2
}

if weapon item group is ammunition
    dmg /= 2
if weapon is Justifier
    if PC is paladin
        dmg *= 2
    else
        dmg /= 2

if weapon is lightened [everburning] torch and monster is vulnerable to fire
    dmg += 2d4

if dmg < 1
    dmg = 1

if (weapon prefix is "chaotic" and monster is lawfull OR
    weapon prefix is "lawfull" and monster is chaotic OR
    weapon prefix is "unbalance" and monster is neutral)
        dmg += 1d8

if weapon prefix is "hatefull" and berserking
   dmg += 2d6

Weapon hit crit is applied here!!

if fighter bash used
    dmg += dmg/5
    
if attacking with everburning torch
    if dmg is bigger than PV leave it be
    otherwise dmg = monster_pv + 1

if weapon has "mild" prefix
    dmg /= 5
    
if weapon has "of penetration" suffix
    dmg += monster_pv

if weapon is phase dagger or
   (PC is properly equipped duelist and
   monster is human but not undead/construct and
   1 in 5) 
   dmg += monster_pv

if monster_PV >= dmg {
    "but do not manage to harm"
} else {
    monster_HP -= (dmg - PV)
    
    if monster died {
        note kill for pc
        if weapon has "of hunting" suffix 1 in 15 to set hunted flag
    } else {
        if weapon is "frozen" or
           weapon is "flaming" and not under ice queen spell or
           weapon is "of lightning" {           
            damage monster by 2d6 using element damage
            if monster dies {
                note kill for pc
                stop processing
            }
        }
        corrupt monster when weapon is "of corruption" or PC has chaos hands corruption
        
        possibility of stunning monster for 1d3 turns //TODO
        possibility of inflicting bleed wounds //TODO
        possibility of poisoning monster //TODO
        if monster has paryzation defense and PC doesn't wear amulet of free action
            TestForParalysis()
        if monster has acid defense {
            can hurt PC
            can damage/destroy gauntlets
            can corode weapon if not wearing gauntlets
        }
        if monster isn't undead/construct and 
           weapon is "of vampirism" or using necromanter power
                restore "some" HP               //TODO
    }
} 
```

### PCMeleeDamageMods

```
attrib = PC_St or PC_De //Dexterity for whips
dmg_mod = 0
if attrib < 9
    dmg_mod -= (attrib-9)/2
if attrib > 12
    dmg_mod += (sttrib-12)/2

if weapon is 2 handed {
    dmg_mod += CK_lvl40_dmg_bonus
    if weapon is 2handed-class and PC_St >= 12 { //not polearm/stave etc
        x = MAX(4, 2_handed_weapon_skill*2) //level 0 and 1 are treated as lvl 2
        x = MIN(x, dmg_mod/2) //dmg_mod from large strength caps weapon skill bonus
        dmg_mod += MAX(1, x)
    }
}

if weapon weigths 100 or more {
    dmg_mod += 1 if has basher talent
    dmg_mod += 2 if has powerfull strike talent
    dmg_mod += 2 if has mighty strike talent
}

when weapon matches PC affinity {
    dmg_mod += 2 axe, clubs/hammers, mace/flail, polearms, staves, swords, 2-hnd-weapon, whips
    dmg_mod += 3 daggers/knives
}

dmg_mod += 6 with mele weapon master
dmg_mod += 2+PC_lvl/20 if PC is blessed
dmg_mod += 3 if PC has horns corruption
dmg_mod += 3 if PC wears brass knuckles and no weapon
dmg_mod += (1, 2, 4) if PC wears braces of boxing and no weapon
dmg_mod += to_dmg from weapon

if PC is beastfighter and no weapon
    dmg_mod += PC_lvl/4

if PC is properly equiped duelist
    dmg_mod +=  Weapon skill rank / 2

if non-artifact spear or bugbiter and other hand is empty
    dmg_mod +=  2

dmg_mod +=  RingDmgMods
dmg_mod +=  smithing gloves bonus

dmg_mod += 6 if true berserker
dmg_mod += 10 if true berserker lvl 40 berserker //...
dmg_mod += 6 if PC has Rage chaos power
dmg_mod += 2 if PC is drunk

if PC is beastmaster with weapon
    dmg_mod /= 4

dmg_mod += weapon skill dmg mod

if overburdened
    worse of 2 is picked: dmg_mod += (dmg_mod+3)/4 or dmg_mod-12

if wielding sting and needle
    dmg_mod += 40

if wielding Rolf's Companion in left hand and Rolf's Saviour in right hand
    dmg_mod += 10

dmg_mod += PCTacticalDamage(dmg_mod)

```

### PCTacticalDamage
```
PCTacticalDamage(dmg_to_mod)

mod = (130, 115, 105, 100, 90, 80, 70) //[berserk, ..., coward]
if Tactics is different from normal
    mod += PC_Skill(Tactics) / 7

if dmg_to_mod < 0
    tactic_dmg_mod = (dmg_to_mod * 100) / mod - dmg_to_mod
else
    tactic_dmg_mod = (dmg_to_mod * mod) / 100 - dmg_to_mod

tactic_dmg_mod += (3, 2, 1, 0, -1, -2, -3)

if PC born in Dragon or Sword month and tactic_dmg_mod > 0
    tactic_dmg_mod = tactic_dmg_mod + (tactic_dmg_mod / 10)
    
if PC has Natural Berserker talent and in berserk
    tactic_dmg_mod += 2

if PC is beastfighter and uses weapon //both base dmg and dmg_mod are / 4 for beastfighters with weapon
    tactic_dmg_mod /= 4

return tactic_dmg_mod

```


### Backstabbing check

```
backstabbing check {
if SkillCheck(Backstabbing, monster_lvl * 3 + 120) AND
   ((monster is friedly or neutral) OR (monster can't see PC AND 1 in 3)) AND
   monster is not undead or construct {
       backstab check passes
   }
}
```

### PCCriticalHitMod - base for weapon and kick crits

```
PCCriticalHitMod:
if PC is unburdened monk
  + PC_lvl / 3
if PC is assassin
  + PC_lvl / 2
+ (PC_St - 13) / 4 //1 for St == 17. 21 for St == 97
```

### Weapon hit crit

```
if weapon slays monster
    PC attack crits
1_in_X = MAX(5, 20 - PCCriticalHitMod() - PC_Luck + PC_Curse)
if weapon is murderous // assasins with murderouis weapons get to 50% very quickly
  1_in_X = 1_in_X / 2
if R(1_in_X) == 0
    PC attack crits
if SkillCheck(Find weakness in 500) //0% to 20% when skill is 100
    PC attack crits
if monster is PLANT and SkillCheck(Woodcraft in 1500) //0% to 6,66% when skill is 100
    PC attack crits
if backstabbing check passed and 1 in 4
    PC attack crits
for each item providing more crits {
    if 1d100 < item_crit_chance
        PC attack crits
}
if PC is fighter lvl 25 or more 10% that PC attack crits
if PC is assasin lvl 25 or more 20% that PC attack crits

if PC is bestmaster attacking animal and R(120) < PC_lvl + 10
  PC attack crits

if PC attack crits
  if staff of undead slaying attacks undead
    dmg is multiplied by [2..5]
  else
    dmg is multiplied by 2
```

### Kick crit
```
1_in_X = MAX(5, 20 - PCCriticalHitMod() - PC_Luck + PC_Curse)
if R(1_in_X) == 0
    PC kick crits
if SkillCheck(Find weakness in 500)
    PC kick crits
if PC is bestmaster and R(120) < PC_lvl + 10
    PC kick crits

if PC kick crits {
    Assassin and Monk can instantly kill monster
    kick_dmg is multiplied by [2..5]
}
```


### Missle weapon crit

```
if weapon or missle slays monster or missle is "of slaying"
  PC rng attack crits
1_in_X = MAX(5, 20 - PCCriticalMissileHitMod() - PC_Luck + PC_Curse)
if SkillCheck(Find weakness in 1000)
  PC rng attack crits

Crit multiplier = 2 + R(4) + 1 if weapon slays + 1 if missle slays //[2..7]
```

```
PCCriticalMissileHitMod
if PC is archer
  + PC_lvl / 3
if PC is assassin
  + PC_lvl / 4
+ (PC_Pe - 13) / 8 //1 for Pe == 21. 10 for Pe == 93
```