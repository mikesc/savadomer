Femate PCs have +5 to notice things about monsters (experiencs, dv, pv, speed, special abilities, special defence, used spells, damage...)

Base cost in Dwarwen smith is 5000 for females and 5250 for males.

## Omens

Function that would change intrinsic isn't called so only 2 omens change anything. 

| Omen description                                                                 | Omen effects                                                                                                       |
|----------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| "A falling star killed the village elder the night you were born"                | PC should be cursed (doesn't work), require 10% less XP (works)                                                    |
| "When you were born, a two-headed calf was born the same night"                  | PC should be cursed (doesn't work), require 10% less XP (works)                                                    |
| "On the night you were born your long-lost brother showed up again"              | PC should be lucky  (doesn't work)                                                                                 |
| "The night you were born, the local undead were seen packing up and moving away" | PC should be doomed, fate smile and enemy of undead (all weapons act as if they were undead slayer) (doesn't work) |

## Resurrection date and time (30 Aug 11:59::00)

If PC is healing at resurrection date and time it can get message "You suddenly feel an extreme boost to your vitality... as if some higher force had given y ou a chance for a second life!"
 after with PC will be totally healed and Toughness and Mana will get pernament + 4

If PC dies at resurrection date and time it will get resurrected. Doesn't work on old age death, only amulet of resurrection helps with that.
