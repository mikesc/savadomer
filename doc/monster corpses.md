## Corpse generation chance

Monsters with corpse generation chance == 0 will never generate one.

```
when fighting in melee {
    if attacking hand weapon has "of hunting" suffix (1 in 15) to generate body
}
when missile used {
    if monster is undead, construct, elemental, demon or jelly missile attacks will not affect body generation

    if not using long bow of hunting and missile has "of hunting" suffix {
        50% to generate body for animal
        1 in 6 for every body else
    } else {
        if BUC(40, 20, 2) in R(100) { // bow of hunting
            100% to generate body for animal
            1 in 3 for every body else
        } else if missle has "of hunting" suffix { // bow throw failed but PC is used "of hunting" missle
            50% to generate body for animal
            1 in 6 for every body else
        }
    }
}

try up to MAX(1, Food_Preservation/30) times {
    gen_corpse_by_chance = 1d100 <= Monster_Corpse_Gen_Chance //There is statue that increases Monster_Corpse_Gen_Chance by 1/3 (rounded down).
    if in middle of farmer corpse quest
        gen_farmer_corpse = 50%

    if gen_corpse_by_chance or gen_farmer_corpse
        generate corpse
}
```

