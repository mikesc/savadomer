### What can be dodged

Energy bolts (are hit by a painful stream of energy, barely avoid the energy blast)
Cone of cold (are hit by a cone of magical ice!, barely avoid the icy blast.)
Shock bolts (Tiny bolts of blue lightning are creeping about your body!, barely avoid the shock bolt.)
Death ray
Magic missiles (are hit by a barrage of glowing balls!, barely evade the magical barrage.)


### Dodge

```
if PC position is visible to PC
    range = 40 + PC_Dx
else
    range = 40 + 80 + PC_Dx

if PC_Dx + PC_Luck - PC_Curse < R(range)
    PC dodges

if PC_Ma + PC_Luck - PC_Curse <= R(1000)
    PC dodges

if PC position is visible to PC
    range = 100
else
    range = 200

r = R(range) + 1
if r != 100 and r <= PC_Speed - 100 //allways 1 in 100 or 1 in 200 chance to fail. Speed of 200 gives 99% protection when PC position is visible.
    PC dodges

dodge_range = 1000 or 2000 when PC position isn't visible
if SkillCheck(Dodge, dodge_range) //dodge 100 gives 10% or 5% chance

alertness_range = 200 or 400 when PC position isn't visible
if SkillCheck(Alertness, alertness_range) //alertness 100 gives 87% or 57% chance to dodge

```