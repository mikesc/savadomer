## PC_Luck calculations

PC_Luck is calculated using potential PC_Luck:

PC luck is R(potential_PC_Luck) with is value from 0 to potential_PC_Luck-1

Potential PC_Luck is calculated using below formula 

PC is blessed? +2

PC has innate luck? +3

PC has innate fate smile? +6

for each item that grands luck + 2

for each item that grands fate smile +4

for each item that increases luck +3

PC has birthday? +2 +R(12)

When PC mana is bigger than R(100):

+MIN(4, R(PC_Mana/10 + 2)) //PC with mana 30 can get maximum value but each mana point more will increase chance for any + and for maximum +

There are customization options that give +2 or +4 to potential PC_Luck

## PC_Curse calculations

PC_Curse is sum of:

PC has innate curse +2d2

PC has innate doomed +3d5

\+ R(items_that_grand_curse * 3)

\+ R(items_that_grand_doomed * 8)

\+ R(200) if save file corrupted

PC intrinsics create floor for bad luck (+2 for cursed, +3 for doomed), good luck floor is always 0.

## Things affected by luck

List is complete but doesn't cover magnitude of influence in most cases.

- PC protection from drowning - each ring of fish and armor of the sea gives 60/80/100 protection. Protected when R(100) < + PC_Luck - PC_Curse
- Disease catching from worm ridden armor
- Triggering trap when opening trapped doors (R(50) < EDL - PC_Luck + PC_Curse)
- Generating new monster. Monster level = EDL + MAX(0, PC_Curse - PC_Luck)
- Kick, missile, melee to hit modification (PC_Luck - PC_Curse. 1 luck = 1 to Hit)
- Critical chance when monster shoots at PC
- Monster critical chance when hitting PC
- Critical chance when kicking monster (pc_damage.md)
- Critical chance when weapon doesn't slay monster (pc_damage.md)
- Critical chance when shooting monster (no slaying ammo/weapon, in pc_damage.md)
- Monster bypassing PV special ability when hitting PC ( R(MAX(2, PC_PV + 20 + PC_Luck * 4 - PC_Curse * 4)) == 0 )
- Chance of monster damaging / destroying armor
- Monster slow down attack success (PC_Mana + PC_Luck - PC_Curse < (1d MonsterEffectivenessOffensive))
- Monster toughness or strength drain special ability (PC_Mana + PC_Luck - PC_Curse < (1d MonsterEffectiveness2Offensive)
- Monster paralyzation special ability chance when PC doesn't have resistance (PC_Willpower + PC_Luck - PC_Curse < R(Monster effectiveness))
- Monster sleep special ability chance when PC doesn't have resistance (PC_Mana + PC_Luck - PC_Curse < R(MonsterEffectivenessOffensive))
- Monster confusion special ability chance (same as spell: PC_Willpower + PC_Luck - PC_Curse + (6 * item count with conf res) < MIN(120, R(Monster magic level))
- Monster disease special ability chance when PC doesn't have immunity ( if R(MonsterEffectivenessOffensive + PC_Luck - PC_Curse) == 0 AND PC_Toughness + PC_Luck - PC_Curse + R(6 * item count with disease res) < R(30))
- Stun chance from very strong attacks
- Scroll of power effect magnitude
- Equipment damage
- Final wand charge wrenching
- Finding traps, triggering traps, training attributes when dealing with traps. Determination of HP % left after violent trap damage.
- Getting petrified by eating gorgon corpse with no petrification resistance (PC dies when PC_Mana + PC_Luck - PC_Curse < R(5000))
- Survival usage
- Closed doors opening
- key destruction / breaking when use
- Bandage usage
- Arena fight gold reward
- Ammunition destruction after use (item_destruction.md)
- Monster curse item spell
  - Cast on PC (R(PC_Mana) + PC_Luck - PC_Curse < R(Monster magic level))
  - Cast on item (PC_Mana + PC_Luck - PC_Curse < R(Monster magic level))
- Monster lower attributes spell success (R(PC_Mana) + PC_Luck - PC_Curse < R(Monster magic level))
- Monster confusion spell success (PC_Willpower + PC_Luck - PC_Curse + (6 * item count with conf res) < MIN(120, R(Monster magic level)) // intrinsic resistance doesn't seem to matter
- Success of teleport others
- Evasion of magic spells
- Instant kill by death ray when no resistance (PC_Mana + PC_Luck - PC_Curse < R(Monster magic level))
