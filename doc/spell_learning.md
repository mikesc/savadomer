## Readning spellbook

```
learn_turn_count = MAX(5, base_spell_cost + 3) //base spell cost is in table below
if in shelves of books room
    learn_turn_count = learn_turn_count / 2

wait learn_turn_count turns // here player can cancel learning because of hunger etc

SkillCheck(Literacy in 100) //if literacy skill check fails nothing else happens. If literacy is 100 there is 1 in 1milion to fail.

spell_learn_chance = SpellLearningChance()  // described below
random = R(100) + 1  // [1,100]

if (spell_learn_chance < random) { //failed to learn
    if spell_learn_chance + spell_fail_help > random {
        nothing happens
    } else {
        BotchSpellLearning(); //Described below
    }
} else {
    LearnSpell(spell_learn_chance - random); // described below
    reduce book_usage_count //book is destroyed when it reaches 0
}
```

## SpellLearningChance

```
learning_chance = class_learn_chance_mod + (PC_Learning * class_learn_multi) + spell_learn_chance_mod

if PC is wizard or necromancer {
    if spell is more popular for priests
        learning_chance -= 15
    else
        learning_chance += 5
}
if PC is druid or priest {
    if spell is more popular for wizards
        learning_chance -= 15
    else
        learning_chance += 5
}

learning_chance -= alcohol_level

if book is cursed {
    learning_chance -= 20;
}
if book is blessed {
    learning_chance += 20;
}

if PC is wizard, priest, druid, elementalist or necromancer {
    learning_chance += (PC_lvl * 2)
} else if PC is not troll or mindcrafter {
    learning_chance += PC_lvl
}

learning_chance += Literacy / 10; // from 0 to 10
learning_chance += Concentration / 5; // from 0 to 20

ip PC is necromancer and spell is frost bolt {
    learning_chance += 40
}
ip PC is druid and spell is lightning bolt {
    learning_chance += 40
}
if PC is born in book month {
    learning_chance += 20
}

```

## LearnSpell(spell_learn_chance - random)

```
if current_spell_knowledge > 30 {
    p1 = MAX(1, PC_Learning/8 ) * (spell_learn_chance - random)
    p1 = MAX(p1, 5) // p1 will at least 5
} else {
    p1a = MAX(1, PC_Learning / 4) * MAX(5, spell_learn_chance - random - current_spell_knowledge)
    if p1a < 20 {
        p1a = 20
    }
    p1b = PC_Learning * 4 + 200
    p1 = MIN(p1a, p1b)
}

p2 = (current_spell_knowledge / 50 + PC_lvl / 3) * -3 + 100 // current spell knowledge and PC level lowers this parameter
clap p2 between 25 and 100

p = p1 * p2 / 100

if PC is born in cup month {
    p = p + (p / 5)
}
if PC has good book learner {
    p = p + (p / 10)
}
if PC has great book learner {
    p = p + (p / 10)
}

if new spell is learned {
    mark PC_Learning by base_spell_cost * 2
} else {
    mark PC_Learning by base_spell_cost / 2
}

p will be addedd to spell knowledge
```
## BotchSpellLearning

Black tome of Alsophocus can't be destroyed.

2 in 11 : spell book is destroyed

1 in 11 : use up to base spell cost * 2 PP

1 in 11 : confuse PC

1 in 11 : stun PC

1 in 11 : damage PC by R(base spell cost)+1, bypasses PV

1 in 11 : book explodes and damages PC if not in Ice Queen domain

1 in 11 : blinds PC

1 in 11 : if teleportation isn't possible confuse PC, otherwise random teleport

1 in 11 : To -1

1 in 11 : Creates pit under PC

## Class spell learning modifiers

| class        | learn chance mod | spell fail help | learn multiplier |
|--------------|------------------|-----------------|------------------|
| FIGHTER      | -20              | 15              | 1                |
| PALADIN      | -5               | 25              | 1.5              |
| RANGER       | -15              | 25              | 1.5              |
| THIEF        | 0                | 20              | 2                |
| ASSASSIN     | -15              | 20              | 1.5              |
| WIZARD       | 30               | 50              | 3                |
| PRIEST       | 30               | 40              | 2.5              |
| BARD         | 5                | 30              | 2                |
| MONK         | -10              | 20              | 1.5              |
| HEALER       | 10               | 35              | 2.333(3)         |
| WEAPONSMITH  | -10              | 20              | 1.5              |
| ARCHER       | -20              | 15              | 1                |
| MERCHANT     | -20              | 20              | 1                |
| FARMER       | -10              | 10              | 1                |
| MINDCRAFTER  | -40              | -10             | 0.25             |
| BARBARIAN    | -30              | -20             | 0.125            |
| DRUID        | 20               | 30              | 2                |
| NECROMANCER  | 20               | 40              | 3                |
| ELEMENTALIST | 20               | 50              | 3                |
| BEASTFIGHTER | 10               | 20              | 1                |
| CHAOS_KNIGHT | 0                | 25              | 1                |
| DUELIST      | -15              | 20              | 1                |

## Spellbook popularity

When learning random spell from black tome of Alsophocus each spell has same probability to be picked.

Book with specific spell generation chance is equal to spell popularity divided by sum of popularities.

| Spell name              | wizard popularity | priest popularity | spell_learn_chance_mod | base_spell_cost |
|-------------------------|-------------------|-------------------|------------------------|-----------------|
| Magic missle            | 200               | 40                | -5                     | 8               |
| Lightning Bolt          | 100               | 100               | -15                    | 12              |
| Identify                | 4                 | 4                 | -20                    | 75              |
| Light                   | 300               | 300               | 5                      | 3               |
| Darkness                | 300               | 300               | 5                      | 4               |
| Cure Light Wounds       | 100               | 200               | -5                     | 5               |
| Knock                   | 200               | 40                | -10                    | 12              |
| Disarm Trap             | 40                | 100               | -10                    | 10              |
| Fireball                | 40                | 4                 | -35                    | 20              |
| Bless                   | 40                | 200               | 0                      | 8               |
| Slow Monster            | 200               | 40                | -10                    | 7               |
| Calm Monster            | 100               | 100               | -15                    | 8               |
| Cure Serious Wounds     | 20                | 100               | -10                    | 10              |
| Cure Critical Wounds    | 20                | 40                | -20                    | 15              |
| Heal                    | 4                 | 20                | -40                    | 40              |
| Cure Disease            | 40                | 200               | -10                    | 10              |
| Neutralize Poison       | 40                | 100               | -15                    | 10              |
| Invisibility            | 100               | 20                | -20                    | 10              |
| Destroy Undead          | 20                | 200               | -10                    | 8               |
| Slow Poison             | 100               | 200               | -5                     | 6               |
| Teleportation           | 100               | 40                | -10                    | 22              |
| Remove Curse            | 40                | 40                | -20                    | 50              |
| Greater Identify        | 4                 | 4                 | -40                    | 100             |
| Earthquake              | 4                 | 20                | -50                    | 80              |
| Revelation              | 40                | 100               | -15                    | 33              |
| Know Alignment          | 40                | 100               | -10                    | 10              |
| Create Item             | 4                 | 4                 | -50                    | 250             |
| Summon Monsters         | 20                | 20                | -30                    | 30              |
| Petrification           | 4                 | 40                | -30                    | 120             |
| Frost Bolt              | 100               | 100               | -15                    | 12              |
| Acid Bolt               | 40                | 40                | -20                    | 15              |
| Fire Bolt               | 100               | 100               | -15                    | 10              |
| Strength Of Atlas       | 100               | 40                | 0                      | 10              |
| Magic Lock              | 100               | 40                | 0                      | 8               |
| Improved Fireball       | 20                | 4                 | -50                    | 30              |
| Farsight                | 40                | 40                | 6                      | 17              |
| Web                     | 100               | 40                | -10                    | 12              |
| Stun Ray                | 100               | 100               | -5                     | 8               |
| Death Ray               | 20                | 20                | -50                    | 100             |
| Magic Map               | 40                | 40                | -25                    | 40              |
| Mystic Shovel           | 20                | 20                | -30                    | 100             |
| Burning Hands           | 100               | 100               | -10                    | 8               |
| Wish                    | 1                 | 1                 | -100                   | 3000            |
| Scare Monster           | 40                | 40                | -18                    | 12              |
| Ice Ball                | 40                | 4                 | -45                    | 25              |
| Lightning Ball          | 40                | 4                 | -55                    | 30              |
| Acid Ball               | 40                | 4                 | -65                    | 35              |
| Sum of all popularities | 3265              | 3413              | -                      | -               |
